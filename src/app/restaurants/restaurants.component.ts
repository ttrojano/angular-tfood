import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms'

import {Restaurant} from './restaurant/restaurant.model';
import {RestaurantsService} from './restaurants.service'

import 'rxjs/add/operator/switchMap'
import 'rxjs/add/operator/do' //logar as chamadas
import 'rxjs/add/operator/debounceTime' //
import 'rxjs/add/operator/distinctUntilChanged'
import 'rxjs/add/operator/catch'
import 'rxjs/add/observable/from'
import {Observable} from 'rxjs/Observable'



@Component({
  selector: 'mt-restaurants',
  templateUrl: './restaurants.component.html'

})
export class RestaurantsComponent implements OnInit {

  restaurants: Restaurant[] = []

  searchForm: FormGroup
  searchControl: FormControl

  constructor(private restaurantsService: RestaurantsService,
              private fb: FormBuilder) { }

  ngOnInit() {

      this.searchControl = this.fb.control('')
      this.searchForm = this.fb.group({
        searchControl: this.searchControl
      })

      this.searchControl.valueChanges
        .debounceTime(500)  //espera um tempo para enviar o evento
        .distinctUntilChanged() //nao envia a req se a ultimo digitado for igual ao anterior
        //.do(searchTerm=> console.log(`q=${searchTerm}`))
        .switchMap(searchTerm =>
           this.restaurantsService
           .restaurants(searchTerm)
           .catch(erro=>Observable.from([])))
        .subscribe(restaurants => this.restaurants = restaurants)

      this.restaurantsService.restaurants()
       .subscribe(restaurants => this.restaurants = restaurants)
       //console.log(this.restaurants)
  }

}
