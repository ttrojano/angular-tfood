class Order {
  constructor(
    public name: string,
    public email: string,
    public address: string,
    public OptionalAddress: string,
    public paymentOption: string,
    public orderItems: OrderItem[] = [],
    public restaurant: string,
    public id?: string
  ){}



}

class OrderItem {

  constructor(public quantity: number, public MenuId: string) {}


}


export {Order, OrderItem}
