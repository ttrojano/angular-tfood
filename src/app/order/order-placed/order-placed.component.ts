import { Component, OnInit } from '@angular/core';
import {OrderService} from '../../order/order.service'
import {Order} from '../../order/order.model';


@Component({
  selector: 'mt-order-placed',
  templateUrl: './order-placed.component.html'
})
export class OrderPlacedComponent implements OnInit {

  orders: Order[] = []

  constructor(private orderService: OrderService) { }

  ngOnInit() {
    this.orderService.orders()
      .subscribe(orders => this.orders = orders)

      console.log(this.orders)
  }


}
