import { Component, OnInit } from '@angular/core';
import {FormGroup, FormBuilder, Validators, AbstractControl} from '@angular/forms'

import {Router} from '@angular/router'

import {RadioOption} from '../shared/radio/radio-option.model'
import {OrderService} from './order.service'
import {CartItem} from '../restaurant-detail/shopping-cart/cart-item.model'
import {Order, OrderItem} from './order.model'
import {LoginService} from './../security/login/login.service'
import {User} from './../security/login/user.model'
     

@Component({
  selector: 'mt-order',
  templateUrl: './order.component.html'
})
export class OrderComponent implements OnInit {

  emailPattern = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i

  numberPattern = /^[0-9]*$/

  orderForm: FormGroup

  delivery: number = 8

  paymentOptions: RadioOption[] = [
    {label: 'Cash', value : 'MON'},
    {label: 'Card', value : 'DEB'},
    {label: 'PayPal', value : 'REF'}

  ]

  constructor(private orderService: OrderService, private loginService : LoginService,
              private router: Router,
              private FormBuilder: FormBuilder) { }

  ngOnInit() {
    this.orderForm = this.FormBuilder.group({
      //name: this.FormBuilder.control('', [Validators.required, Validators.minLength(5)]),
      name: this.FormBuilder.control(''),
      //email: this.FormBuilder.control('', [Validators.required, Validators.pattern(this.emailPattern)]),
      //emailConfirmation: this.FormBuilder.control('', [Validators.required, Validators.pattern(this.emailPattern)]),
      address: this.FormBuilder.control('',[Validators.required]),
      optionalAddress: this.FormBuilder.control(''),
      paymentOption: this.FormBuilder.control('', [Validators.required])

    }, {validator: OrderComponent.equalsTo})
  }

  static equalsTo(group:AbstractControl): {[key:string]:boolean}{
    const email = group.get('email')
    const emailConfirmation = group.get('emailConfirmation')

    if(!email || !emailConfirmation) {
      return undefined
    }
    if(email.value !== emailConfirmation.value)  {
      return {emailsNotMatch:true}
    }
      return undefined
  }

  itemsValue(): number {
    return this.orderService.itemsValue()
  }

  cartItems(): CartItem[]{
    return this.orderService.cartItems()
  }

  increaseQty(item:CartItem){
    this.orderService.increaseQty(item)
  }

  decreaseQty(item:CartItem){
    this.orderService.decreaseQty(item)
  }

  remove(item: CartItem){
    this.orderService.remove(item)
  }

  checkOrder(order: Order){
    order.orderItems = this.cartItems()
       .map((item:CartItem)=>new OrderItem(item.quantity, item.menuItem.id))
      this.orderService.checkOrder(order)
       .subscribe((orderId: string) => {
        this.router.navigate(['/order-summary'])
       console.log(`Compra concluída: ${orderId}`)
       this.orderService.clear()
     })
    console.log(order)
  }
  user(): User {
    return this.loginService.user
  }


}
