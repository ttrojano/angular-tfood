export interface User {
  name: string,
  email: string,
  password: string,
  accessToken: string
}

export interface UserAdd {
  name: string,
  email: string
    
}
